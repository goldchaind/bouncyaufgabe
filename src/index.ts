import {GraphQLServer} from "graphql-yoga";
import "reflect-metadata";
import {buildSchema} from "type-graphql"

import SongResolver from "./resolvers/SongResolver";

import {id, Repository} from 'mongodb-typescript';
import {ObjectId, MongoClient} from 'mongodb';

async function startGraphQL(){
    const schema = await buildSchema({
        resolvers:[SongResolver],
        emitSchemaFile:true,
    });

    const server =new GraphQLServer({schema});

    server.start(()=>console.log("Server is running on http://localhost:4000"));
}


startGraphQL();
