import {Field, Int, ObjectType} from "type-graphql";

@ObjectType()
export  class Song{
    @Field()
    ISRC:string;

    @Field(type=> [String])
    Artists:String[];

    @Field()
    Name:string;

    @Field()
    ReleaseDate:string;
}

@ObjectType()
export  class DBSong{
    @Field()
    _id:string;

    @Field()
    ISRC:string;

    @Field(type=> [String])
    Artists:String[];

    @Field()
    Name:string;

    @Field()
    ReleaseDate:string;

    @Field()
    spotifyid:string;

    @Field()
    searchdate:string;
}