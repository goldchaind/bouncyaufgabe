import {Arg, FieldResolver,Mutation, Query, Resolver, Root} from "type-graphql";
import {Song,DBSong} from "../schemas/Song";
import * as request from "request";

import {id, Repository} from 'mongodb-typescript';
import {ObjectId, MongoClient, Db} from 'mongodb';
import { promises } from "dns";

function getSongDataFromSpotify(id:String):Promise<Song>{
    var client_id = 'f053316bf86a4251b308f3ead2cc4123'; // Your client id
        var client_secret = 'eb5606add89a4cd3986676baefe074e8'; // Your secret

// your application requests authorization
        var authOptions = {
            url: 'https://accounts.spotify.com/api/token',
            headers: {
            'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
        },
        form: {
            grant_type: 'client_credentials'
        },
        json: true
        };
        var songpromise:Promise<Song>=new Promise<Song>((resolve, reject) => {
            try{
            request.post(authOptions, function(error, response, body) {
                if (!error && response.statusCode === 200) {
              
                  // use the access token to access the Spotify Web API
                  var token = body.access_token;
                  var options = {
                    url: 'https://api.spotify.com/v1/tracks/',//3n3Ppam7vgaVa1iaRUc9Lp
                    headers: {
                      'Authorization': 'Bearer ' + token
                    },
                    json: true
                  };
                  options.url+=id;
                  try{
                  request.get(options, function(error, response, body) {
                      try{
                    //console.log(body);
                    var ret=new Song();
                    ret.Name=body.name;
                    ret.Artists=[];
                    body.artists.forEach((element:any) => {
                        ret.Artists.push(element.name);
                    });

                    ret.ReleaseDate=body.album.release_date;
                    ret.ISRC=body.external_ids.isrc;
                    resolve(ret);
                }catch(e){
                    reject("Error parsing the Data from Spotify. Maybe your ID is invalid?");
                }
    
                    
                  });
                }catch(e){
                    reject("Error getting the Song Data from Spotify. Maybe your ID is invalid?");
                }
                }
              });
            }catch(e){
                reject("Error trying to authorize to Spotify");
            }

        });       


          return songpromise;
}

const connectionstring: string ="mongodb://localhost:27017";
async function saveDataInDB(id:String,song:Song):Promise<void>{
    var client: MongoClient;
    try{
            
            client= await MongoClient.connect(connectionstring,{useNewUrlParser:true,useUnifiedTopology:true});
            var bouncy:Db = client.db("bouncy");

            var songwithdateandid:any=song;
            songwithdateandid.spotifyid=id;
            songwithdateandid.searchdate=new Date();
            await bouncy.collection("searches").insert(songwithdateandid);
            return Promise.resolve();
            
    }catch(error){
       return Promise.reject();
    }

}

async function getSavedSearchesFromDb():Promise<DBSong[]>{
    var client: MongoClient;
    try{
           
            client= await MongoClient.connect(connectionstring,{useNewUrlParser:true,useUnifiedTopology:true});
            var bouncy:Db = client.db("bouncy");
            var collection=await bouncy.collection("searches").find({}).toArray();
            var collectionJSON=JSON.stringify(collection);
            console.log(collectionJSON);
            var ret:DBSong[]=JSON.parse(collectionJSON);
            return Promise.resolve(ret);
            
    }catch(error){
       return Promise.reject("An Error occured while reading from the DB"+error.toString());
    }
}

@Resolver(of =>Song)
export default class{

    @Query(returns => [DBSong])
    async getSavedSearches():Promise<DBSong[]>{
        return getSavedSearchesFromDb();
    }

    @Mutation(returns =>Song)
    async getSongById(@Arg("id") id:String):Promise<Song>
    {
        try{
            var songData: Song =await getSongDataFromSpotify(id);
            await saveDataInDB(id,songData);
            return songData;
        }catch(e){
            return Promise.reject(e);
        }
        
    }



}