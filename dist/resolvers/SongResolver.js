"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const Song_1 = require("../schemas/Song");
const request = require("request");
const mongodb_1 = require("mongodb");
function getSongDataFromSpotify(id) {
    var client_id = 'f053316bf86a4251b308f3ead2cc4123'; // Your client id
    var client_secret = 'eb5606add89a4cd3986676baefe074e8'; // Your secret
    // your application requests authorization
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
        },
        form: {
            grant_type: 'client_credentials'
        },
        json: true
    };
    var songpromise = new Promise((resolve, reject) => {
        try {
            request.post(authOptions, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    // use the access token to access the Spotify Web API
                    var token = body.access_token;
                    var options = {
                        url: 'https://api.spotify.com/v1/tracks/',
                        headers: {
                            'Authorization': 'Bearer ' + token
                        },
                        json: true
                    };
                    options.url += id;
                    try {
                        request.get(options, function (error, response, body) {
                            try {
                                //console.log(body);
                                var ret = new Song_1.Song();
                                ret.Name = body.name;
                                ret.Artists = [];
                                body.artists.forEach((element) => {
                                    ret.Artists.push(element.name);
                                });
                                ret.ReleaseDate = body.album.release_date;
                                ret.ISRC = body.external_ids.isrc;
                                resolve(ret);
                            }
                            catch (e) {
                                reject("Error parsing the Data from Spotify. Maybe your ID is invalid?");
                            }
                        });
                    }
                    catch (e) {
                        reject("Error getting the Song Data from Spotify. Maybe your ID is invalid?");
                    }
                }
            });
        }
        catch (e) {
            reject("Error trying to authorize to Spotify");
        }
    });
    return songpromise;
}
const connectionstring = "mongodb://localhost:27017";
function saveDataInDB(id, song) {
    return __awaiter(this, void 0, void 0, function* () {
        var client;
        try {
            client = yield mongodb_1.MongoClient.connect(connectionstring, { useNewUrlParser: true, useUnifiedTopology: true });
            var bouncy = client.db("bouncy");
            var songwithdateandid = song;
            songwithdateandid.spotifyid = id;
            songwithdateandid.searchdate = new Date();
            yield bouncy.collection("searches").insert(songwithdateandid);
            return Promise.resolve();
        }
        catch (error) {
            return Promise.reject();
        }
    });
}
function getSavedSearchesFromDb() {
    return __awaiter(this, void 0, void 0, function* () {
        var client;
        try {
            client = yield mongodb_1.MongoClient.connect(connectionstring, { useNewUrlParser: true, useUnifiedTopology: true });
            var bouncy = client.db("bouncy");
            var collection = yield bouncy.collection("searches").find({}).toArray();
            var collectionJSON = JSON.stringify(collection);
            console.log(collectionJSON);
            var ret = JSON.parse(collectionJSON);
            return Promise.resolve(ret);
        }
        catch (error) {
            return Promise.reject("An Error occured while reading from the DB" + error.toString());
        }
    });
}
let default_1 = class default_1 {
    getSavedSearches() {
        return __awaiter(this, void 0, void 0, function* () {
            return getSavedSearchesFromDb();
        });
    }
    getSongById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var songData = yield getSongDataFromSpotify(id);
                yield saveDataInDB(id, songData);
                return songData;
            }
            catch (e) {
                return Promise.reject(e);
            }
        });
    }
};
__decorate([
    type_graphql_1.Query(returns => [Song_1.DBSong]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], default_1.prototype, "getSavedSearches", null);
__decorate([
    type_graphql_1.Mutation(returns => Song_1.Song),
    __param(0, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], default_1.prototype, "getSongById", null);
default_1 = __decorate([
    type_graphql_1.Resolver(of => Song_1.Song)
], default_1);
exports.default = default_1;
//# sourceMappingURL=SongResolver.js.map